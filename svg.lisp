(in-package :cl-user)

(defun generate-svg ()
  (let ((horizontal
         "Horizontal Text")
        (file (make-pathname :defaults (asdf:system-relative-pathname :generate-svg "generated-example.svg"))))
    (svg:with-svg-to-file 
        (scene 'svg:svg-1.1-toplevel :height "11in" :width "8.5in")
        (file :if-exists :supersede)
      (svg:make-svg-symbol scene (:id "label")
        (svg:text* (:x "5%" :y "95%" 
                       :width "100%" :height "100%"
                       :text-length "100%"
                       :length-adjust "spacingAndGlyphs")
                   horizontal))
      (svg:draw scene (:use :xlink-href "#label")
                :x 0 :y 0))
    file))


#| cribbed from illithid which has a BSD copyright UBSDM University at Buffalo School of Dental Medicine
(defun generate-svg ()
  (svg:with-svg-to-file
      (scene 'svg:svg-1.1-toplevel :height "11in" :width "8.5in")
      (file  :if-exists :supersede)
    (add-label-symbols scene 
                       :horizontal horizontal
                       :vertical vertical
                       :qr-uri qr-uri)
    (loop 
         :for across :to 7
         :with top-margin = .5
         :with left-margin = .25
         :doing (loop 
                   :for down :to 9
                   :doing (svg:draw scene (:use :xlink-href "#label")
                                    :height "1in"
                                    :width "1in"
                                    :x (format nil "~Ain" (+ across left-margin))
                                    :y (format nil "~Ain" (+ down top-margin)))))))

(defun add-label-symbols (scene 
                          &key
                            horizontal
                            vertical
                            qr-uri)
  "Add necessary symbols for a qr:label to SCENE."
  (let ((horizontal
         (if horizontal
             horizontal
             "Patient, Joe"))
        (vertical
         (if vertical
             vertical
             "C314159"))
        (qr-uri
         (if qr-uri
             qr-uri
             "file:///no-such-image.png")))
    (svg:make-svg-symbol scene (:id "label")
      (svg:draw*  (:rect :x 0 :y 0 :height "100%" :width "100%") 
                  :stroke (get-label-color) 
                  :stroke-width "1" :fill "none") 
      (svg:draw*  (:image :x "10%" :y "10%" :width "70%" :height "70%"
                          :xlink-href qr-uri))
      (svg:transform ((svg:rotate -90 70 65))
        (svg:text* (:x "65%" :y "90%" 
                       :stroke (get-label-color)) 
                   vertical))
      (svg:text* (:x "5%" :y "95%" 
                     ;; :fill (get-label-color)
                     :width "100%" :height "100%"
                     :text-length "100%"
                     :length-adjust "spacingAndGlyphs")
                 horizontal))))
|#
